package com.ing.hackathon.controller;

import java.text.ParseException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.ing.hackathon.dto.DeviceInformation;
import com.ing.hackathon.service.LocationService;

@RestController
@CrossOrigin("*")
public class LocationController {
	private final Logger log = LoggerFactory.getLogger(LocationController.class);

	@Autowired
	LocationService locationService;

	@RequestMapping(value = "/location/click", method = RequestMethod.POST,consumes={ "application/json" })
	public ResponseEntity<String> getCustomerClick(@RequestBody @Valid DeviceInformation deviceInformation) throws ParseException, JsonProcessingException {
		log.info("**** deviceInformation *****  "+ deviceInformation);
		boolean flag=locationService.clickhandler(deviceInformation);

		String response = "fail";
		if(flag){
			response= "success";
		}
			 
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}

}
