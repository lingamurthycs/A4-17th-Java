package com.ing.hackathon.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeviceInformation {

	@JsonProperty
	private Long customerId;
	@JsonProperty
	private String deviceId;
	@JsonProperty
	private String deviceLatitude;
	@JsonProperty
	private String deviceLongitude;
	@JsonProperty
	private String geofenceId;
	@JsonProperty
	private String timeStamp;

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceLatitude() {
		return deviceLatitude;
	}

	public void setDeviceLatitude(String deviceLatitude) {
		this.deviceLatitude = deviceLatitude;
	}

	public String getDeviceLongitude() {
		return deviceLongitude;
	}

	public void setDeviceLongitude(String deviceLongitude) {
		this.deviceLongitude = deviceLongitude;
	}

	public String getGeofenceId() {
		return geofenceId;
	}

	public void setGeofenceId(String geofenceId) {
		this.geofenceId = geofenceId;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public String toString() {
		return "DeviceInformation [customerId=" + customerId + ", deviceId=" + deviceId + ", deviceLatitude="
				+ deviceLatitude + ", deviceLongitude=" + deviceLongitude + ", geofenceId=" + geofenceId
				+ ", timeStamp=" + timeStamp + "]";
	}

}
