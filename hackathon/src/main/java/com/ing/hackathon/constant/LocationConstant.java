package com.ing.hackathon.constant;

import java.util.HashMap;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class LocationConstant {
	public HashMap<String, Integer> outerMap = new HashMap<String, Integer>();
	public HashMap<String, Integer> innerMap = new HashMap<String, Integer>();
	/*
	 * public HashMap<String, HashMap<String, Integer>> getOuterMap() { return
	 * outerMap; } public void setOuterMap(HashMap<String, HashMap<String,
	 * Integer>> outerMap) { this.outerMap = outerMap; }
	 */

	public HashMap<String, Integer> getInnerMap() {
		return innerMap;
	}

	public HashMap<String, Integer> getOuterMap() {
		return outerMap;
	}

	public void setOuterMap(HashMap<String, Integer> outerMap) {
		this.outerMap = outerMap;
	}

	public void setInnerMap(HashMap<String, Integer> innerMap) {
		this.innerMap = innerMap;
	}

}
