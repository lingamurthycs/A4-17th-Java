package com.ing.hackathon.service;

import java.text.ParseException;

import com.ing.hackathon.dto.DeviceInformation;

public interface LocationService {
	public boolean clickhandler(DeviceInformation deviceInformation) throws ParseException;
}
