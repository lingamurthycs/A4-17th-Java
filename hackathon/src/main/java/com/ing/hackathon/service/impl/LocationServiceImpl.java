package com.ing.hackathon.service.impl;

import java.text.ParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ing.hackathon.constant.LocationConstant;
import com.ing.hackathon.dto.DeviceInformation;
import com.ing.hackathon.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService {
	private final Logger log = LoggerFactory.getLogger(LocationServiceImpl.class);

	private static final int TIMELLIMIT = 180000;

	@Autowired
	private LocationConstant locationConstant;

	@Override
	public boolean clickhandler(DeviceInformation deviceInformation) throws ParseException {
		boolean flag = false;
		// SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd
		// hh:mm:ss:SSS");

		if (!locationConstant.getOuterMap()
				.containsKey(deviceInformation.getCustomerId() + deviceInformation.getGeofenceId())) {
			// Date date = new Date();

			// locationConstant.getInnerMap().put(dateFormat.format(date), 1);
			// locationConstant.getOuterMap().put(deviceInformation.getCustomerId()
			// + deviceInformation.getGeofenceId(), 1);
			locationConstant.getOuterMap().put(deviceInformation.getCustomerId() + deviceInformation.getGeofenceId(),
					1);

		} else {
			// HashMap<String, Integer> innerMap1 =
			// locationConstant.getOuterMap().get(deviceInformation.getCustomerId()
			// + deviceInformation.getGeofenceId());
			Integer value = locationConstant.getOuterMap()
					.get(deviceInformation.getCustomerId() + deviceInformation.getGeofenceId());

			locationConstant.getOuterMap().put(deviceInformation.getCustomerId() + deviceInformation.getGeofenceId(),
					++value);
			if (value == 3) {
				flag = true;
			}

			// Iterator it = innerMap1.entrySet().iterator();
			/*
			 * while (it.hasNext()) { Map.Entry pair = (Map.Entry) it.next();
			 * 
			 * String timestampString = (String) pair.getKey(); Integer count =
			 * (Integer) pair.getValue();
			 * 
			 * Date parsedDate = dateFormat.parse(timestampString);
			 * 
			 * Date timeStamp1 = new Date();
			 * 
			 * long diff = timeStamp1.getTime() - parsedDate.getTime();
			 * 
			 * log.info("Difference  = " + diff);
			 * 
			 * }
			 */
		}
		return flag;
	}

}
