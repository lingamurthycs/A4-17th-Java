package com.ing.hackathon.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the customer_offer database table.
 * 
 */
@Entity
@Table(name="customer_offer")
@NamedQuery(name="CustomerOffer.findAll", query="SELECT c FROM CustomerOffer c")
public class CustomerOffer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="offer_id")
	private String offerId;

	@Column(name="expiration_time")
	private Timestamp expirationTime;

	@Column(name="generated_time")
	private Timestamp generatedTime;

	@Column(name="place_address")
	private String placeAddress;

	@Column(name="place_id")
	private String placeId;

	@Column(name="place_name")
	private String placeName;

	private String state;

	//bi-directional many-to-one association to DeviceInformation
	@ManyToOne
	@JoinColumn(name="customer_id")
	private DeviceInformation deviceInformation;

	public CustomerOffer() {
	}

	public String getOfferId() {
		return this.offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	public Timestamp getExpirationTime() {
		return this.expirationTime;
	}

	public void setExpirationTime(Timestamp expirationTime) {
		this.expirationTime = expirationTime;
	}

	public Timestamp getGeneratedTime() {
		return this.generatedTime;
	}

	public void setGeneratedTime(Timestamp generatedTime) {
		this.generatedTime = generatedTime;
	}

	public String getPlaceAddress() {
		return this.placeAddress;
	}

	public void setPlaceAddress(String placeAddress) {
		this.placeAddress = placeAddress;
	}

	public String getPlaceId() {
		return this.placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getPlaceName() {
		return this.placeName;
	}

	public void setPlaceName(String placeName) {
		this.placeName = placeName;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public DeviceInformation getDeviceInformation() {
		return this.deviceInformation;
	}

	public void setDeviceInformation(DeviceInformation deviceInformation) {
		this.deviceInformation = deviceInformation;
	}

}