package com.ing.hackathon.model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the device_information database table.
 * 
 */
@Entity
@Table(name="device_information")
@NamedQuery(name="DeviceInformation.findAll", query="SELECT d FROM DeviceInformation d")
public class DeviceInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="customer_id")
	private String customerId;

	@Column(name="device_id")
	private String deviceId;

	@Column(name="device_lat")
	private String deviceLat;

	@Column(name="device_long")
	private String deviceLong;

	private String geofenceid;

	private Timestamp timestamp;

	//bi-directional many-to-one association to CustomerOffer
	@OneToMany(mappedBy="deviceInformation")
	private List<CustomerOffer> customerOffers;

	public DeviceInformation() {
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getDeviceId() {
		return this.deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getDeviceLat() {
		return this.deviceLat;
	}

	public void setDeviceLat(String deviceLat) {
		this.deviceLat = deviceLat;
	}

	public String getDeviceLong() {
		return this.deviceLong;
	}

	public void setDeviceLong(String deviceLong) {
		this.deviceLong = deviceLong;
	}

	public String getGeofenceid() {
		return this.geofenceid;
	}

	public void setGeofenceid(String geofenceid) {
		this.geofenceid = geofenceid;
	}

	public Timestamp getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public List<CustomerOffer> getCustomerOffers() {
		return this.customerOffers;
	}

	public void setCustomerOffers(List<CustomerOffer> customerOffers) {
		this.customerOffers = customerOffers;
	}

	public CustomerOffer addCustomerOffer(CustomerOffer customerOffer) {
		getCustomerOffers().add(customerOffer);
		customerOffer.setDeviceInformation(this);

		return customerOffer;
	}

	public CustomerOffer removeCustomerOffer(CustomerOffer customerOffer) {
		getCustomerOffers().remove(customerOffer);
		customerOffer.setDeviceInformation(null);

		return customerOffer;
	}

}